# The task is to configure two upstream nginx servers and their backend - grafana:
- make the first nginx to forward port 8001
- make the second nginx to forward port 8001
- the network for nginx1 differs from the network for nginx2
- create the one and the only volume with config for both nginx servers
***

# The result
- we can run curl from the host to two addresses and their ports and see the output from grafana server